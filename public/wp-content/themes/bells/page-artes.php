
<?php
// Template Name: Artes
?>
<?php get_header(); ?>

		<section class="sec_artes">
			<H2>Artes</H2>

			<div class="artes">
					<ul>
						<li><img src="<?php the_field("imagem_1"); ?>" alt=""></li>
						<li><img src="<?php the_field("imagem_2"); ?>" alt=""></li>
						<li><img src="<?php the_field("imagem_3"); ?>" alt=""></li>
						<li><img src="<?php the_field("imagem_4"); ?>" alt=""></li>
						<li><img src="<?php the_field("imagem_5"); ?>" alt=""></li>
						<li><img src="<?php the_field("imagem_6"); ?>" alt=""></li>
					</ul>
			</div>
		</section>

<?php get_footer(); ?>
