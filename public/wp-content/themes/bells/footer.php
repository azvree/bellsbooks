		<footer>

				<div id="footerinfo">
					<nav>
						<ul>
							<li><a href="/sobre/">Sobre</a></li>
							<li><a href="/artes/">Artes</a></li>
							<li><a href="/livros/">Livros</a></li>
						</ul>
						<ul>
							<li><a href="/contato/">Contato</a></li>
							<li><a href="/login/">Login</a></li>
						</ul>
						<div>
						<ul>
							<li><a href="https://www.instagram.com" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ant-design_instagram-outlined.png" alt="icone do instagram"></a></li>
							<li><a href="https://www.facebook.com" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/iconedoface.png" alt="icone do facebook"></a></li>
							<li><a href="https://www.twitter.com" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/iconedotwit.png" alt="icone do twitter"></a></li>
							<li><a href="https://www.pinterest.com" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cib_pinterest-p.png" alt="icone do pinterest"></a></li>

						</ul>
						</div>
					</nav>
					<div id="footerlateral">
						<p>Desenvolvimento por:</p>
						<center><a href="https://injunior.com.br/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logoIN.png" alt="INjunior"></a></center>
					</div>
				</div>

		</footer>
		<p id="footercopright">© Copyright 2020 Bell’s Books</p>
		<?php wp_footer(); ?>
	</body>
</html>