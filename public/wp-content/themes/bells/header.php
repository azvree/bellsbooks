<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		
		<title>Bells Book</title>
		<link href="https://fonts.googleapis.com/css?family=Overlock+SC|UnifrakturCook:700|UnifrakturMaguntia&display=swap" rel="stylesheet">
		
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">
		<?php wp_head(); ?>
	</head>

	<body>
		
		<header>
			<h1>Bell's</h1>
			<nav>
				<ul>
					<li><a href="/livros/">Livros</a></li>
					<li><a href="/artes/">Artes</a></li>
					<li><a href="/sobre/">Sobre</a></li>
					<li><a href="/contato/">Contato</a></li>
					<li><a href="/login/">Login</a></li>
					<li><a id="doar" href="/doar/">Doar</a></li>
				</ul>
				
			</nav>
		</header>