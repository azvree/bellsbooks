<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'NLCZj3JCu8XRcCuyO15b1ePyFPV0R6WdP1KtKDDTKLBVbtX8QBZyM3hjn7ZVmBiQLcsD4oeksC1Mh4uHME+0Dg==');
define('SECURE_AUTH_KEY',  'MuhSr6c2m0R/A3Vtgf8+yYFY7tDmpdSMtZnkCKVBvBhPz7l6q46cGIZeI1PfJD64RGhZr7w2E47wobtOFmevCQ==');
define('LOGGED_IN_KEY',    'RrikLUe6U8LSCD2I15UGacQsutnngR+wyB06zTwcmfWXXFYW1jW/6AErZBjAHmLurEV5QVHZ37kcfWzZOf7vpw==');
define('NONCE_KEY',        'NlC2wRMewMH5gSoIUHVS+910hzIjRiA8Z95i6uvNqR6dojqeOuKIsfin604Y9anwEdFhbEDhAH1L8/25t4aZvQ==');
define('AUTH_SALT',        'X3ruVx95qYs/816FW2lHidSMf8bC28z5FWPQ3YkBkiQK/ixEI64EHWmDkrUaAfl11ZmqVSkr+W+XZndOwvM6+A==');
define('SECURE_AUTH_SALT', 'WWFUx6asuMPN8icjP5hkn89TNUmRf+EnMgZ4o8JQeOp8LDKDaNrlYyN8cgARjDmj2zZxf1OGHg8MR53Wh+m7tw==');
define('LOGGED_IN_SALT',   'eblqcZtMglX5CUhrL9yd9O0F9wv3m8VaRKKV7C/QonPsqbF0H3aXv01s3EiDaDKRPIqR/ChH/1vvP54GR9fibA==');
define('NONCE_SALT',       'LZPKDXbxFprx45OJL8cEJrhgAWc2821afZZ/Abb4esB2OZNGzmeicr/J/2yH9lV9/kQSQLt8PUsovVO/9QUewA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
